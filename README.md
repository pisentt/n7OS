# Projet kernel - n7OS
**Téo Pisenti**

## Travail réalisé

- Console
- Interruptions: handler générique qui passe le numéro de l'interruption en paramètre à un handler en C (kernel/test_irq.c)
- Pagination: activer la pagination désactive le blue screen de base. J'ai donc écrit mon propre handler pour le `segmentation fault`
- Horloge
- Appel système: write implanté, printf utilise write.
- Processus:
    - Ordonancement explicite selon la politique fifo. Une fonction wrapper permet de gérer la terminaison des processus et le processus `kernel_start` est automatiquement débloqué si la fifo est vide.
    - Round Robin était trop buggé. Appeler `scheduler` dans le handler du timer fait qu'on sort jamais réellement du handler (pas d'appel à `reti`). Cela pose problème quand un processus se termine ou lorsqu'on revient à un ancien processus. J'ai expérimenté de remplacer l'adresse de retour du handler par scheduler (registre ebp) puis d'exécuter `reti` mais cela causait des bugs. La bonne solution semble être d'écrire un nouveau `ctx_sw` directement dans le handler assembleur et d'appeler `reti` mais j'ai plus le temps.
- include/manipulation_bits.h : module personnel pour manipuler des bits facilement.
- lib|include/fifo.*: module de file FIFO de pid_t que j'ai écrit.
