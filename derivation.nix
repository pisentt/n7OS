{ stdenv, gnumake, qemu, gcc-unwrapped }:

stdenv.mkDerivation rec {
  name = "n7OS-${version}";
  version = "0.1";

  src = ./.;

  nativeBuildInputs = [
    gcc-unwrapped
    gnumake
  ];

  buildInputs = [
    qemu
  ];

}
