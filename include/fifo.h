/** Module de fifo d'entiers écrit par Téo Pisenti */

/** Taille maximale de la fifo */
#include "n7OS/processus.h"
#define FIFOLEN 1024

typedef struct _fifo {
  pid_t contenu[FIFOLEN];
  int debut; /* indice de la tete du fifo */
  int fin; /* indice de la queue du fifo */
} fifo_t;


/** initialiser une nouvelle fifo vide
 * @param fifo un pointeur sur la structure qui va être initialisée */
void init_fifo(fifo_t* fifo);

/** insère un nouvel élément en queue du fifo
* @param fifo le fifo dans lequel on insère
* @param val la valeur à insérer
* @return 0 si tout va bien, -1 si le fifo est plein */
int fifo_enqueue(fifo_t* fifo, pid_t val);

/** retire l'élément en tête du fifo
* @param fifo le fifo dans lequel on retire
* @param val un pointeur dans lequel on va écrire la valeur en tête du fifo
* @return 0 si tout va bien, -1 si le fifo est vide */
int fifo_dequeue(fifo_t* fifo, pid_t* val);

/** retire toutes les occurrences de la valeur demandée dans la fifo
 * @param val valeur à retirer
 * @return le nombre d'éléments supprimés */
int fifo_remove(fifo_t* fifo, pid_t val);
