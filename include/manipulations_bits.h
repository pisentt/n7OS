/** Bibliothèque de manipulations binaires d'entiers par Téo Pisenti */
#ifndef __MANIP_BITS_H__
#define __MANIP_BITS_H__

#include "inttypes.h"
#include <malloc.h>
#include <string.h>

/* on définit la taille du byte à 8 pour manipuler les bits */
#define TAILLE_BYTE 8

/* le type des segments du bitmap */
#define BITMAP_SEGMENT_TYPE uint32_t

/* type utilisé pour représenter un bitmap
 * (en général, de la taille du byte) */
typedef BITMAP_SEGMENT_TYPE* bitmap_t;

/** creer un nombre avec que des 1 de la même taille
 * que le type passé en paramètre */
#define FULL_1(type) (~((type) 0x0))

/** créer un masque binare de la moitiée de la taille de n */
#define MASQUE_POIDS_FAIBLES(n) ((1 << ((TAILLE_BYTE / 2) * sizeof(n))) - 1)

/** récupère la moitiée de poid faible des bits de n */
#define POIDS_FAIBLES(n) ((n) & MASQUE_POIDS_FAIBLES(n))

/** récupère la moitiée de poid fort des bits de n */
#define POIDS_FORTS(n) (((n) >> ((TAILLE_BYTE / 2) * sizeof(n))) & MASQUE_POIDS_FAIBLES(n))

/** récupérer le bit d'indice i (en partant de la gauche) de n */
#define GET_BIT(n, i) (((n) >> (TAILLE_BYTE * sizeof(n) - (i) - 1)) & 0x1)

/** creer un entier de la meme taille que n où tous les bits sont 0
 * sauf le bit d'indice i qui est égal à 'bit' */
#define BIT_I(n, i, bit) ((bit) << (TAILLE_BYTE * sizeof(n) - (i) - 1))

/** affecte 0 au i-eme bit en partant de la gauche de la variable n
 * et renvoie le nouveau n
 * NE MODIFIE PAS LE n D'ORIGINE */
#define EFFACER_BIT(n, i) ((n) & (~BIT_I(n, i, 0x1)))

/** set la valeur du i-eme bit en partant de la gauche de la variable n
 * et renvoie le nouveau n
 * NE MODIFIE PAS LE n D'ORIGINE */
#define SET_BIT(n, i, bit) (EFFACER_BIT(n, i) + BIT_I(n, i, bit))

/** la taille d'un élément du tableau en bits (ici: 8 bits) */
#define TAILLE_UNITE_BITMAP (TAILLE_BYTE * sizeof(BITMAP_SEGMENT_TYPE))

/** renvoie 1 si le segment de taille = TAILLE_UNITE_BITMAP
 * où se trouve le ieme bit du bitmap ne contient que des 1
 * -> permet de faire des recherches de bit=0 plus rapidement
 *    (pour éviter de parcourir tous les bits) */
#define BITMAP_IS_SEGMENT_FULL(bmp, i) \
  (bmp[i / TAILLE_UNITE_BITMAP] == (BITMAP_SEGMENT_TYPE) FULL_1(BITMAP_SEGMENT_TYPE))

/** taille du tableau d'éléments utilisé pour représenter le bitmap
 * "(n + TAILLE_UNITE_BITMAP - 1)" permet de faire un arrondi à la valeur au-dessus */
#define BITMAP_TAILLE_TABLEAU(n) ((n + TAILLE_UNITE_BITMAP - 1) / TAILLE_UNITE_BITMAP)

/** créer un nouveau bitmap de taille n bits, tous initialement à 0
 * NE SURTOUT PAS OUBLIER DE FREE !! */
#define BITMAP_CREATE(bmp, n) (bmp = (bitmap_t) malloc( \
      BITMAP_TAILLE_TABLEAU(n) * sizeof(BITMAP_SEGMENT_TYPE)))

/** mettre tous les bits du bitmap de taille n à la valeur de bit */
#define BITMAP_FILL(bmp, n, bit) \
  (memset(bmp, bit * FULL_1(BITMAP_SEGMENT_TYPE), \
          BITMAP_TAILLE_TABLEAU(n) * sizeof(BITMAP_SEGMENT_TYPE)))

/** set la valeur du i-eme bit du bitmap */
#define BITMAP_SET(bmp, i, bit) (bmp[i / TAILLE_UNITE_BITMAP] = \
    SET_BIT(bmp[i / TAILLE_UNITE_BITMAP], i % TAILLE_UNITE_BITMAP, bit))

/** obtenir la valeur du i-eme bit du bitmap */
#define BITMAP_GET(bmp, i) GET_BIT(bmp[i / TAILLE_UNITE_BITMAP], i % TAILLE_UNITE_BITMAP)

#endif
