#ifndef __PROCESSUS_H__
#define __PROCESSUS_H__

#include "inttypes.h"

typedef uint32_t pid_t;

/* EN_COURS : le processus est ELU ou PRET
 * BLOQUE : le processus est stoppé mais lancé
 * LIBRE : cette structure ne contient pas encore de processus */
typedef enum process_etat { LIBRE, EN_COURS, BLOQUE } process_etat_t;

/* La taille de la pile d'un processus (nombre d'entiers de 64bits) */
#define STACK_SIZE 1024

/* La taille de la table des processus */
#define NB_PROCS 256

/** initialiser les processus */
void init_processus();

/** Créer et lance une fonction dans un nouveau processus
 * @param
 * @return le pid du nouveau pid ou -1 si plus de processus libres */
int creer_processus(void * fonction);

/** Changer le processus qui tourne par le suivant dans la fifo
 * ATTENTION: Le processus actuel sera stoppé et les lignes suivant l'appel de
 * cette fonction ne seront exécutées qui si on change à nouveau de processus. */
void scheduler();

/** Libérer les ressources d'un processus fini */
void detruire_processus(pid_t pid);

/** Bloquer un processus
 * @param pid le pid du processus à bloquer */
void bloquer_processus(pid_t pid);

/* fonction basique pour tester les processus en affichant 1 */
void test_function();

/* fonction basique pour tester les processuss en affichant 2 */
void test_function2();

#endif
