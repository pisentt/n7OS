#ifndef __SYSCALL_DEFS_H__
#define __SYSCALL_DEFS_H__

#define NB_SYSCALL 3

int sys_example();

int sys_shutdown(int n);

/** implémentation de l'appelle système write exécutée en kernel mode
 * @param s la chaine de caractères à afficher
 * @param len le nombre de caractères à afficher
 * @return nombre écrits si tout va bien ou -1 si erreur
 */
int sys_write ( const char *s , int len );

typedef int (*fn_ptr)();
extern fn_ptr syscall_table[NB_SYSCALL];

void add_syscall(int num, fn_ptr function);

#endif
