/** Fonctions pour créer le timer */
#ifndef TIMER_H
#define TIMER_H

#include "inttypes.h"
#include <manipulations_bits.h>
#include <n7OS/cpu.h>

/* Fréquence de l'oscillateur hardware */
#define F_OSC 0x1234BD

/* Fréquence de l'horloge à générer */
#define F_HORLOGE 1000

/* Adresse du channel 0 du générateur d'horloge */
#define TIMER_CHANNEL0 0x40

/* Adresse du registre de controle du générateur d'horloge */
#define TIMER_REG_CONTROLE 0x43

/* Adresse du port PIC pour la configuration */
#define PORT_PIC_DATA 0x21

/* Adresse du port PIC pour les commandes */
#define PORT_PIC_COMMANDE 0x20

/* Constantes pour controler le générateur d'horloge */
#define TIMER_MODE_CHANNEL0 0x00
#define TIMER_MODE_PD_FAIBLEFORT 0x30
#define TIMER_MODE_INTERRUPT 0x4
#define TIMER_MODE_BINAIRE 0

/** Initialiser l'horloge système */
void init_timer();

/** Handler en C appelé à chaque interruption levée par le timer */
void handler_horloge();

/** Afficher l'uptime actuel en haut à droit de l'écran */
void afficher_uptime();

#endif
