#include "inttypes.h"
#include "string.h"
#include <n7OS/console.h>
#include <n7OS/timer.h>
#include <n7OS/cpu.h>
#include <manipulations_bits.h>

/** macro pour afficher un caractère printable dans la couleur du terminal */
#define COLORED_CHAR(c) CHAR_COLOR << 8 | c;

/** macro pour décrémenter le curseur en faisant attention
 * à ne pas être < 0 */
#define DECREMENTER_CURSEUR(pos) ((pos == 0)?(0):(pos - 1))

/** le buffer du texte */
static uint16_t *scr_tab; /* static pour qu'elle soit visible
                             que dans ce fichier */

/** Remonter tous les caractères d'une ligne
 * en supprimant le contenu de l'ancienne première ligne */
void remonter_ecran() {
  /* on décale tout le contenu existant d'une ligne vers le haut */
  memcpy(scr_tab, scr_tab + VGA_WIDTH,
         sizeof(uint16_t) * (VGA_HEIGHT - 1) * VGA_WIDTH);

  /* effacer la nouvelle ligne */
  memset(scr_tab + (VGA_HEIGHT - 1) * VGA_WIDTH,
         0, sizeof(uint16_t) * VGA_WIDTH);

  afficher_uptime();
}

/** Positionner le curseur à la position demandée de l'écran
 * @param pos la nouvelle position du curseur
 */
void set_cursor(uint16_t pos) {

  /* on remonte l'écran si la position est en dehors des limites */
  for(uint16_t i = pos / (VGA_HEIGHT * VGA_WIDTH); i > 0; i--) {
    remonter_ecran();
    pos -= VGA_WIDTH;
  }

  outb(CMD_LOW, PORT_CMD);
  outb(POIDS_FAIBLES(pos), PORT_DATA);
  outb(CMD_HIGH, PORT_CMD);
  outb(POIDS_FORTS(pos), PORT_DATA);
}

/** Récupérer la position du curseur
 * @return la position actuelle du curseur
 */
uint16_t get_cursor() {
  /** la position du curseur */
  uint16_t pos;

  /* On récupère d'abord les bits de poids faible 
     puis les bits de poids fort du curseur en demandant au hardware */
  outb(CMD_LOW, PORT_CMD);
  pos = inb(PORT_DATA);
  outb(CMD_HIGH, PORT_CMD);
  pos += ((uint16_t) inb(PORT_DATA)) << 8;

  return pos;
}

/** Initialiser la console */
void init_console() {
  scr_tab= (uint16_t *) SCREEN_ADDR;
}

/** Afficher un caractère à l'écran à la position du curseur
 * @param c le caractère a afficher ou un caractère spécial
 *          qui va déplacer le curseur
 */
void console_putchar(const char c) {
  uint16_t pos = get_cursor();

  switch (c) {
    case '\b':
      /* reculer le curseur d'une case */
      set_cursor(DECREMENTER_CURSEUR(pos));
      break;
    case '\n':
      /* placer le curseur a la ligne suivante */
      set_cursor((pos / VGA_WIDTH + 1) * VGA_WIDTH);
      break;
    case '\r':
      /* placer le curseur au début de la ligne courante */
      set_cursor((pos / VGA_WIDTH) * VGA_WIDTH);
      break;
    default:
      if (31 < c && c < 127) {
        /* caractère printable, on l'affiche à la position courante */
        scr_tab[pos] = COLORED_CHAR(c);
        set_cursor(pos + 1); /* on avance le curseur */
      }
  }
}

void console_putbytes(const char *s, int len) {
  for (int i= 0; i<len; i++) {
    console_putchar(s[i]);
  }
}
