#include "n7OS/processor_structs.h"
#include "n7OS/segment.h"
#include <inttypes.h>
#include <n7OS/irq.h>
#include <manipulations_bits.h>

/** ajouter un handler à la table des interruptions
 * @param irq_num le numéro du handler dans la table
 * @param addr l'addresse (le label) du handler en assembleur
 */
void init_irq_entry(int irq_num, uint32_t addr) {

  idt_entry_t ligne;
  ligne.offset_sup = (uint16_t) POIDS_FORTS(addr);
  ligne.offset_inf = (uint16_t) POIDS_FAIBLES(addr);
  ligne.zero = 0x00;
  ligne.type_attr = 0b10000000 | 14; /* P=1 + S=0 + interrupt gate=14 */
  ligne.sel_segment = KERNEL_CS;

  /* conversion de la struct en un entier 64 bits. peut avoir des risques
   * si on mélange big endian ou little endian ? */
  idt[irq_num] = *((uint64_t*) &ligne);
}
