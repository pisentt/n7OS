#include "inttypes.h"
#include "n7OS/kheap.h"
#include "types.h"
#include <n7OS/mem.h>
#include <manipulations_bits.h>

/** le bitmap pour savoir les pages libres */
static bitmap_t bmp;

/** le début de l'espace mémoire réservé pour toutes les pages */
static uint32_t adresse_premiere_page;

/** transformer un indice du bitmap en adresse physique */
#define PAGE_INDEX_TO_ADR(i) (adresse_premiere_page + (i) * PAGE_SIZE)

/** transformer une adresse de page en indice du bitmap */
#define PAGE_ADR_TO_INDEX(adr) (((adr) - adresse_premiere_page) / PAGE_SIZE)


/**
 * @brief Marque la page allouée
 * 
 * Lorsque la page a été choisie, cette fonction permet de la marquer allouée
 * 
 * @param addr Adresse de la page à allouer
 */
void setPage(uint32_t addr) {
  BITMAP_SET(bmp, PAGE_ADR_TO_INDEX(addr), PAGE_ALLOUE);
}

/**
 * @brief Désalloue la page
 * 
 * Libère la page allouée.
 * 
 * @param addr Adresse de la page à libérer
 */
void clearPage(uint32_t addr) {
  BITMAP_SET(bmp, PAGE_ADR_TO_INDEX(addr), PAGE_LIBRE);
}

/**
 * @brief Fourni la première page libre de la mémoire physique tout en l'allouant
 * 
 * @return uint32_t Adresse de la page sélectionnée
 */
uint32_t findfreePage() {
  uint32_t index = 0;
  while (BITMAP_IS_SEGMENT_FULL(bmp, index)) {
    index += TAILLE_UNITE_BITMAP;
    if (index >= NOMBRE_PAGES)
      goto not_enough_memory;
  } while (BITMAP_GET(bmp, index)) {
    index++;
    if (index >= NOMBRE_PAGES)
      goto not_enough_memory;
  }

  BITMAP_SET(bmp, index, PAGE_ALLOUE);
  return PAGE_INDEX_TO_ADR(index);

  not_enough_memory:
    __asm__ __volatile__("int $(14)");
    return 0;
}

/**
 * @brief Initialise le gestionnaire de mémoire physique
 * 
 */
void init_mem() {
  bmp = (bitmap_t) kmalloc_a(BITMAP_TAILLE_TABLEAU(NOMBRE_PAGES) * sizeof(BITMAP_SEGMENT_TYPE));
  BITMAP_FILL(bmp, NOMBRE_PAGES, 0);
  // adresse_premiere_page = kmalloc_a(PAGE_SIZE * NOMBRE_PAGES);
  adresse_premiere_page = 0;
}

/**
 * @brief Affiche l'état de la mémoire physique
 * 
 */
void print_mem() {
  int n = 0;
  for (size_t i = 0; i < NOMBRE_PAGES; i++) {
    n += BITMAP_GET(bmp, i);
  }
  printf("nombre de pages utilisées = %d\n", n);
}
