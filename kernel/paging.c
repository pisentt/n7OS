#include <n7OS/paging.h>
#include <n7OS/kheap.h>
#include <n7OS/mem.h>
#include "inttypes.h"
#include "string.h"
#include "stdio.h"
#include "types.h"

PageDirectory pageDirectory;

#define BIT_PAGING 0x80000000;

// void handler_page_fault(registers_t reg) {
//   // A page fault has occurred.
//   // The faulting address is stored in the CR2 register.
//   uint32_t faulting_address;
//   __asm__ __volatile__("mov %%cr2, %0" : "=r" (faulting_address));
// 
//   // The error code gives us details of what happened.
//   int present = !(reg.err_code & 0x1); // Page not present
//   int rw = reg.err_code & 0x2;           // Write operation?
//   int us = reg.err_code & 0x4;           // Processor was in user-mode?
//   int reserved = reg.err_code & 0x8;     // Overwritten CPU-reserved bits of page entry?
//   int id = reg.err_code & 0x10;          // Caused by an instruction fetch?
// 
//   // Output an error message.
//   printf("Page fault! ( ");
//   if (present) {printf("present ");}
//   if (rw) {printf("read-only ");}
//   if (us) {printf("user-mode ");}
//   if (reserved) {printf("reserved ");}
//   if (id) {printf("instruction ");}
//   printf(") at 0x%x\n", faulting_address);
//   PANIC("Page fault");
// }


/** initialiser une entrée de la table des pages avec les paramètres donnés */
void setPageEntry(PTE *page_table_entry, uint32_t new_page, int is_writeable, int is_kernel) {
  page_table_entry->page_entry.present= 1;
  page_table_entry->page_entry.accessed= 0;
  page_table_entry->page_entry.dirty= 0;
  page_table_entry->page_entry.rw= is_writeable;
  page_table_entry->page_entry.user= ~is_kernel;
  page_table_entry->page_entry.page= new_page>>12;
}


void initialise_paging() {

  /* marqueur pour savoir où s'arrête la mémoire actuellement
   * allouée au kernel */
  uint32_t index= 0;

  /* initialiser la mémoire physique */
  init_mem();

  pageDirectory= (PageDirectory) kmalloc_a (sizeof(PDE) * 1024);
  memset(pageDirectory, 0, sizeof(PDE) * 1024);

  for (size_t i = 0; i < 1024; ++i) {
    // PageTable new_page_table = (PageTable) findfreePage();
    PageTable new_page_table= (PageTable) kmalloc_a(sizeof(PTE)*1024);
    memset(new_page_table, 0, sizeof(PTE) * 1024);
    pageDirectory[i].dir_entry.present = 1;
    pageDirectory[i].dir_entry.rw = 1;
    pageDirectory[i].dir_entry.page_table = 0xfffff & (((uint32_t) new_page_table) >> 12);
    index= (uint32_t) new_page_table + sizeof(PTE) * 1024;
  }

  /* IMPORTANT: on mappe toute la mémoire actuellement utilisée
   * sinon on pourra plus y accéder après activation de la pagination */
  for (size_t i = 0; i < index; i += PAGE_SIZE) {
    alloc_page_entry(i, 1, 1);
  }

  // register_interrupt_handler(14, handler_page_fault);

  /* load l'adresse du page directory dans le registre cr3 */
  __asm__ __volatile__("mov %0, %%cr3" : : "r" (pageDirectory));

  /* activer le paging en mettant à 1 le bon bit */
  uint32_t cr0;
  __asm__ __volatile__("mov %%cr0, %0" : "=r" (cr0));
  cr0 = cr0 | BIT_PAGING;
  __asm__ __volatile__("mov %0, %%cr0" : : "r" (cr0));

  /*
 mov eax, page_directory
 mov cr3, eax

 mov eax, cr0
 or eax, 0x80000001
 mov cr0, eax
 */
}

PageTable alloc_page_entry(uint32_t address, int is_writeable, int is_kernel ) {
  /* address = adresse virtuelle à allouer 
   * address = idx_PDE | idx_PTE | offset
   *             10    |    10   |   12
   */

  PageTable page_table;

  /* on recupere l'entree dans le répertoire de page
  * une entree contient l'adresse de la table de page + bits de controle */
  PDE page_dir_entry = pageDirectory[(address >> 22)]; 

  /* on recupere l'adresse de la page de table dans le répertoire de page */
  page_table = (PTE*) (page_dir_entry.dir_entry.page_table << 12);

  /* recherche d'une page libre dans la memoire physique */
  uint32_t phy_page = findfreePage(); 

  /* mise a jour de l'entree dans la page de table */
  setPageEntry(&page_table[0x3ff & (address >> 12)],
               phy_page, is_writeable, is_kernel);

  return page_table;
}
