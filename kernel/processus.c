#include "inttypes.h"
#include "n7OS/cpu.h" // pour avoir cli
#include "string.h"
#include "unistd.h"
#include <n7OS/processus.h>
#include <stdio.h>
#include <fifo.h>

/* On importe la fonction assembleur pour changer de contexte */
extern void ctx_sw(void *ctx_old, void *ctx_new);

/** Wrapper pour les fonctions lancées dans le processus pour réaliser certaines
 * opérations et/ou récupérer la valeur de retour à la fin du processus */
void wrapper_processus(void (*function)(void), pid_t pid);

typedef struct processus {
  /* Dernière sauvegarde des registres du processus */
  uint32_t regs[5];

  uint32_t stack[STACK_SIZE]; /* La pile du processus */

  pid_t pid; /* Le pid du processus */

  process_etat_t etat; /* L'état courant du processus */
} processus_t;

/** La table des processus */
processus_t table_processus[NB_PROCS];

/** Le pid du processus courant */
pid_t running_process;

/** La file des pid processus pour l'ordonnancement */
fifo_t fifo_process;

/** initialiser les processus */
void init_processus() {
  /* On initialise la table des processus, par défaut l'état est LIBRE (0) */
  memset(table_processus, 0, NB_PROCS * sizeof(processus_t));
  /* On considère que le pid du processus premier (kernel_start) est 0 */
  running_process = 0;
  /* On initialise la fifo */
  init_fifo(&fifo_process);

  /* On crée un processus spécial pour stocker le kernel_start si on schedule
   * On a pas besoin de créer de stack car on utilise celle du kernel,
   * le pid est déjà à 0 grace au memeset
   * et pas besoin d'initialiser les registres car le processus "tourne" déjà */
  table_processus[0].etat = BLOQUE; /* mode bloqué pour exécuter uniquement les taches.
                                       L'ordonnancement repasse automatiquement
                                       la main au kernel_start quand tous les
                                       autres processus sont finis */
}


/** Trouver le prochain pid libre
 * @param pid l'endroit où écrire le nouveau pid
 * @return -1 si plus de processus libres, 0 sinon */
int trouver_pid_libre(pid_t *pid) {
  for (int i = 0; i < NB_PROCS; i++) {
    if (table_processus[i].etat == LIBRE) {
      *pid = i;
      return 0;
    }
  }
  return -1; /* Plus de pid disponible, erreur */
}


/** Créer un nouveau processus qui est bloqué pour le moment
 * @param fonction la fonction qu'exécute le processus
 * @return le pid du nouveau pid ou -1 si plus de processus libres */
int creer_processus(void * fonction) {
  pid_t pid;

  if (trouver_pid_libre(&pid) < 0)
    return -1;
  else {
    processus_t* proc = table_processus + pid;
    proc->pid = pid;
    proc->regs[1] = (uint32_t) (proc->stack + STACK_SIZE - 4);
    proc->stack[STACK_SIZE - 4] = (uint32_t) wrapper_processus; /* fonction lancée par ctx_sw */
    proc->stack[STACK_SIZE - 2] = (uint32_t) fonction; /* argument 1 de wrapper_processus */
    proc->stack[STACK_SIZE - 1] = (uint32_t) pid; /* argument 2 de wrapper_processus */

    proc->etat = EN_COURS;

    fifo_enqueue(&fifo_process, pid);
    return pid;
  }
}


/** Changer le processus qui tourne par le suivant dans la fifo
 * ATTENTION: Le processus actuel sera stoppé et les lignes suivant l'appel de
 * cette fonction ne seront exécutées qui si on change à nouveau de processus. */
void scheduler() {
  pid_t old = running_process;
  if (table_processus[old].etat == EN_COURS) {
    /* On remet le processus dans la file que s'il est encore
     * en cours d'exécution */
    fifo_enqueue(&fifo_process, old);
  }
  if (fifo_dequeue(&fifo_process, &running_process) < 0) {
    /* si la file est vide, on repasse au processus
     * du kernel même s'il est bloqué */
    running_process = 0;
    table_processus[0].etat = EN_COURS;
  }
  ctx_sw(table_processus[old].regs, table_processus[running_process].regs);
  /* tout ce qui suit ne sera jamais exécuté */
}


/** Libérer les ressources d'un processus fini */
void detruire_processus(pid_t pid) {
  memset(table_processus + pid, 0, sizeof(processus_t));
}


/** Bloquer un processus
 * @param pid le pid du processus à bloquer */
void bloquer_processus(pid_t pid) {
  table_processus[pid].etat = BLOQUE;
  fifo_remove(&fifo_process, pid);
  if (running_process == pid)
    scheduler();
}


/** Wrapper pour les fonctions lancées dans le processus pour réaliser certaines
 * opérations et scheduler automatiquement à la fin d'un processus */
void wrapper_processus(void (*function)(void), pid_t pid) {
  printf("******************** processus debut ***********************\n");
  function();
  printf("********************* processus fini ***********************\n");

  table_processus[pid].etat = LIBRE;
  scheduler();
}


/** fonction basique pour tester les processus */
void test_function() {
  for (int j = 0; j < 15; j++) {
    printf("proc%d|%d\n",1,j);
    for (int i = 0; i < 1000; i++) {
      hlt();
    }
    scheduler(); /* ordonnancement explicite */
  }
}


/** fonction basique pour tester les processus */
void test_function2() {
  for (int i = 0; i < 10; i++) {
    printf("proc%d|%d\n",2,i);
    for (int j = 0; j < 1000; j++) {
      hlt();
    }
    scheduler(); /* ordonnancement explicite */
  }
}
