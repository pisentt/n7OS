#include "n7OS/irq.h"
#include "n7OS/mem.h"
#include "n7OS/sys.h"
#include "unistd.h"
#include <n7OS/cpu.h>
#include <inttypes.h>
#include <n7OS/processor_structs.h>
#include <n7OS/console.h>
#include <stdio.h>
#include <n7OS/paging.h>
#include <n7OS/processus.h>
#include <n7OS/timer.h>


void kernel_start(void)
{
  setup_base(0 /* la memoire virtuelle n'est pas encore definie */);

  /* activer la pagination */
  initialise_paging();

  /* lancement des interruptions */
  sti();

  /* ajouter le handler de l'interruption de test */
  init_irq();

  /* initialiser l'affichage */
  init_console();

  /* activer les appels système */
  init_syscall();

  /* initialiser l'horloge */
  init_timer();

  printf("bonjour, ceci est un test\ndu framebuffer");

  printf("petit test un peueee\b\b, long........\n......\nfini\n");

  printf("0123456789");
  printf("0123456789");
  printf("0123456789");
  printf("0123456789");
  printf("0123456789");
  printf("0123456789");
  printf("0123456789");
  printf("0123456789");
  printf("0123456789");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("0123456789\rHello\n a");

  __asm__ __volatile__("int $(50)");
  __asm__ __volatile__("int $(51)");
  __asm__ __volatile__("int $(52)");
  __asm__ __volatile__("int $(53)");
  __asm__ __volatile__("int $(54)");
  __asm__ __volatile__("int $(55)");
  __asm__ __volatile__("int $(56)");
  __asm__ __volatile__("int $(57)");
  __asm__ __volatile__("int $(58)");
  __asm__ __volatile__("int $(59)");
  __asm__ __volatile__("int $(60)");

  print_mem();

  uint32_t* num = (uint32_t *) 5320700; /* juste avant le segfault */
  //num = (uint32_t *) 0xA000000; /* tester le segfault */
  *num = 42;
  printf("test memoire = %d\n", *num);

  // num = (uint32_t *) 5320701; /* valeur limite de ce qui est actuellement alloué */
  // *num = 42; /* doit déclencher un segfault !! */
  // printf("test memoire = %d\n", *num); /* jamais affiché */

  if (example() == 1) {
    printf("appel system exemple ok\n");
  }

  /* doit renvoyer 4 */
  printf("valeur de n = %d\n", shutdown(4));

  /* tester l'appel système write */
  write("petit test de l'appel systeme write\n", 36);


  /* tester de lancer un processus */
  init_processus();
  printf("creation des processus");
  pid_t pid = creer_processus(test_function);
  printf("pid=%d\n", pid);
  pid = creer_processus(test_function2);
  printf("pid=%d\n", pid);

  scheduler();

  printf("\n************* PLUS DE PROCESSUS : RETOUR AU KERNEL ****************\n");

  // on ne doit jamais sortir de kernel_start
  while (1) {
    // cette fonction arrete le processeur
    hlt();
  }
}
