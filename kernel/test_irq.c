#include "inttypes.h"
#include "n7OS/timer.h"
#include <n7OS/irq.h>
#include <n7OS/cpu.h>
#include <stdio.h>

/** L'id de la première interruption de test */
#define INT_TEST_DEBUT 14

/** Le nombre d'interruptions de test définies */
#define INT_TEST_NUMBER 47

/** La liste des handlers d'interruption (en asm dans handler_IT.S) */
extern uint32_t handlers_IT[INT_TEST_NUMBER];

/** Ajouter l'interruption dans la table des interruptions */
void init_irq() {
  for (int i = 0; i < INT_TEST_NUMBER; i++) {
    init_irq_entry(INT_TEST_DEBUT + i, handlers_IT[i]);
  }
}

/** Le code en C appelé par le handler en assembleur */
void handler_en_C(unsigned int id) {
  // SOLUTION TEMPORAIRE POUR LE SEGFAULT !!
  if (id == 14) {
    /* handler du segfault */
    printf("SEGMENTATION FAULT !! KERNEL PANIC !!");
    // on ne doit jamais sortir du kernel panic
    while (1) {
      // cette fonction arrete le processeur
      hlt();
    }
  } else if (id == 32) {
    handler_horloge();
    /* handler de l'horloge */
  } else {
    printf("RECU INTERRUPTION : %d\n", id);
  }
  return;
}
