/** Fonctions pour créer le timer */
#include "inttypes.h"
#include <manipulations_bits.h>
#include <n7OS/cpu.h>
#include <n7OS/timer.h>
#include <n7OS/console.h>


static uint32_t ticks = 0;


/** Afficher l'uptime actuel en haut à droit de l'écran */
void afficher_uptime() {
  uint16_t oldpos = get_cursor();
  set_cursor(VGA_WIDTH - 15);
  printf("uptime: %ds", ticks / 1000);
  set_cursor(oldpos);
}


void init_timer() {
  /* définir une fréquence de la milliseconde */
  static uint16_t frequence = F_OSC / F_HORLOGE;
  outb(TIMER_CHANNEL0 | TIMER_MODE_PD_FAIBLEFORT | TIMER_MODE_INTERRUPT |
       TIMER_MODE_BINAIRE, TIMER_REG_CONTROLE);
  outb(POIDS_FAIBLES(frequence), TIMER_CHANNEL0);
  outb(POIDS_FORTS(frequence), TIMER_CHANNEL0);

  /* démasquer l'interruption du timer */
  outb(inb(PORT_PIC_DATA) & (~1), PORT_PIC_DATA);
}


void handler_horloge() {
  /* ack de l'interruption de l'horloge */
  outb(0x20, PORT_PIC_COMMANDE);

  /* incrementer et afficher */
  if (((ticks++) % 1000) == 0) {
    afficher_uptime();
    return 0;
  }
  return 1;
}
