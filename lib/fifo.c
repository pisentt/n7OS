/** Module pour faire un fifo implanté par un tableau qui se replie sur lui-même */
#include "n7OS/processus.h"
#include <fifo.h>

/** initialiser une nouvelle fifo vide
 * @param fifo un pointeur sur la structure qui va être initialisée */
void init_fifo(fifo_t* fifo) {
  /* si les deux indices sont à -1 => file vide */
  fifo->debut = -1;
  fifo->fin = -1;
}

/** insère un nouvel élément en queue du fifo
* @param fifo le fifo dans lequel on insère
* @param val la valeur à insérer
* @return 0 si tout va bien, -1 si le fifo est plein */
int fifo_enqueue(fifo_t* fifo, pid_t val) {
  int new_fin = (fifo->fin + 1) % FIFOLEN; /* toujours >= 0 */
  
  if (new_fin == fifo->debut)
    return -1; /* la file est pleine */
  else {
    fifo->contenu[new_fin] = val;
    fifo->fin = new_fin;

    if (fifo->debut < 0)
      fifo->debut = fifo->fin; /* si la file était vide, on change le début */
    return 0;
  }
}

/** retire l'élément en tête du fifo
* @param fifo le fifo dans lequel on retire
* @param val un pointeur dans lequel on va écrire la valeur en tête du fifo
* @return 0 si tout va bien, -1 si le fifo est vide */
int fifo_dequeue(fifo_t* fifo, pid_t* val) {
  if (fifo->debut < 0)
    return -1; /* la file est vide : erreur */

  *val = (fifo->contenu)[fifo->debut];
  if (fifo->debut == fifo->fin) {
    /* si c'était le dernier élement de la file,
     * on doit retransformer en la file vide */
    fifo->debut = -1;
    fifo->fin = -1;
  } else
    fifo->debut = (fifo->debut + 1) % FIFOLEN;
  return 0;
}

/** retire toutes les occurrences de la valeur demandée dans la fifo
 * @param val valeur à retirer
 * @return le nombre d'éléments supprimés */
int fifo_remove(fifo_t* fifo, pid_t val) {
  int decalage = 0;
  int i = fifo->debut;
  int j = i;
  int fini = 0;
  while (!fini) {
    fini = (j == fifo->fin);
    if (fifo->contenu[i] == val) {
      decalage++;
      j = (i + decalage) % FIFOLEN;
      fifo->contenu[i] = fifo->contenu[j];

      if (fifo->debut == fifo->fin) {
        /* si c'était le dernier élement de la file,
         * on doit retransformer en la file vide */
        fifo->debut = -1;
        fifo->fin = -1;
      } else {
        fifo->fin = (fifo->fin - 1) % FIFOLEN;
      }
    } else {
      i = (i + 1) % FIFOLEN;
      j = (i + decalage) % FIFOLEN;
    }
  }

  return decalage;
}
