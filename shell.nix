{ pkgs ? import <nixpkgs> {  } }:
let
  derivation = pkgs.callPackage ./derivation.nix {};
in
pkgs.stdenv.mkDerivation {
  name = "env-${derivation.name}";

  buildInputs = derivation.buildInputs ++  (with pkgs; [ clang-tools gdb man-pages man-pages-posix ]);

  nativeBuildInputs = derivation.nativeBuildInputs;
}
