#include <stddef.h>
#include <stdio.h>
#include "../../include/manipulations_bits.h"

/* le but c'est de tester un bitfield pour voir si je peux utiliser ça
 * pour gérer les adresses de la mémoire virtuelle */
/* ATTENTION: le résultat peut dépendre de l'endian du proco */
struct adresse_t {
  int offset : 12; /* offset dans la page */
  int index_pte : 10; /* index table des pages */
  int index_pde : 10; /* index répertoire des pages */
};

union adresse_u {
  struct adresse_t bits;
  int value;
};

typedef union adresse_u adresse_u;

int main() {
  printf("================ DEBUT DU TEST ================\n");

  printf("obtenu %d, attendu %d\n", BIT_I(uint8_t, 4, 1), 8);
  // printf("debug : %lu\n", (TAILLE_BYTE * sizeof(uint8_t) - (4) - 1));

  uint8_t test = 15;
  test = SET_BIT(test, 7, 0);
  test = SET_BIT(test, 1, 1);

  printf("obtenu %d, attendu %d\n", test, 78);
  printf("obtenu %d, attendu %d\n", GET_BIT(test, 5), 1);

  bitmap_t bmp;
  BITMAP_CREATE(bmp, 10);

  printf("obtenu %d, attendu %d\n", (int) BITMAP_TAILLE_TABLEAU(10), 1);

  BITMAP_FILL(bmp, 10, 1);

  printf("obtenu ");
  for (size_t i = 0; i < 10; i++) {
    printf("%d", BITMAP_GET(bmp, i));
  }
  printf(", attendu %s", "1111111111\n");

  BITMAP_FILL(bmp, 10, 0);

  // printf("debug : %lu\n", (10 + TAILLE_UNITE_BITMAP - 1) / TAILLE_UNITE_BITMAP);

  BITMAP_SET(bmp, 0, 1);
  BITMAP_SET(bmp, 1, 1);
  BITMAP_SET(bmp, 5, 1);
  BITMAP_SET(bmp, 7, 1);
  BITMAP_SET(bmp, 8, 1);
  BITMAP_SET(bmp, 9, 1);

  // printf("debug : %lu\n", 9 / TAILLE_UNITE_BITMAP(bmp));

  printf("obtenu ");
  for (size_t i = 0; i < 10; i++) {
    printf("%d", BITMAP_GET(bmp, i));
  }
  printf(", attendu %s", "1100010111\n");

  BITMAP_SET(bmp, 3, 0);
  BITMAP_SET(bmp, 5, 0);
  BITMAP_SET(bmp, 7, 0);
  BITMAP_SET(bmp, 8, 0);
  BITMAP_SET(bmp, 9, 1);

  printf("obtenu ");
  for (size_t i = 0; i < 10; i++) {
    printf("%d", BITMAP_GET(bmp, i));
  }
  printf(", attendu %s", "1100000001\n");

  /* le résultat peut changer en fonction de la taille du BYTE */
  printf("obtenu %d, attendu 255\n", (unsigned char) FULL_1(unsigned char));

  for (size_t i = 0; i < TAILLE_UNITE_BITMAP; i++) {
    BITMAP_SET(bmp, i, 1);
  }

  printf("obtenu %d, attendu 1\n", BITMAP_IS_SEGMENT_FULL(bmp, 0));
  // printf("obtenu %d, attendu 0\n",
  //        BITMAP_IS_SEGMENT_FULL(bmp, TAILLE_UNITE_BITMAP));

  /* ne pas oublier de free le bitmap */
  free(bmp);

  /* test des bitfields */
  adresse_u adr;
  adr.bits.index_pte = 0xab;
  printf("debug: %lu\n", sizeof(adr));
  printf("debug: %lu\n", sizeof(int));
  printf("debug: %x\n", adr.value);
}
