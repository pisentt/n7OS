{ stdenv, gcc }:

stdenv.mkDerivation rec {
  name = "test-fuse-${version}";
  version = "1.0";

  src = ./.;

  nativeBuildInputs = [
    gcc
  ];

  # buildInputs = [
  # ];

  buildPhase = ''
     gcc -Og -ggdb -fanalyzer fifo.c
  '';

  dontStrip = true;

  installPhase = ''
    mkdir -p $out/bin
    cp a.out $out/bin/
  '';
}
