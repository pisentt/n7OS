#include <stdio.h>
#include <stdlib.h>
#define EXIT_SUCCESS 0

/** Module pour faire un fifo implanté par un tableau qui se replie sur lui-même */

#define BUFLEN 5

typedef struct _fifo {
  int contenu[BUFLEN];
  int debut; /* indice de la tete du fifo */
  int fin; /* indice de la queue du fifo */
} fifo_t;

/** initialiser une nouvelle fifo vide
 * @param fifo un pointeur sur la structure qui va être initialisée */
void init_fifo(fifo_t* fifo) {
  /* si les deux indices sont à -1 => file vide */
  fifo->debut = -1;
  fifo->fin = -1;
}

/** insère un nouvel élément en queue du fifo
* @param fifo le fifo dans lequel on insère
* @param val la valeur à insérer
* @return 0 si tout va bien, -1 si le fifo est plein */
int fifo_enqueue(fifo_t* fifo, int val) {
  int new_fin = (fifo->fin + 1) % BUFLEN; /* toujours >= 0 */
  
  if (new_fin == fifo->debut)
    return -1; /* la file est pleine */
  else {
    fifo->contenu[new_fin] = val;
    fifo->fin = new_fin;

    if (fifo->debut < 0)
      fifo->debut = fifo->fin; /* si la file était vide, on change le début */
    return 0;
  }
}

/** retire l'élément en tête du fifo
* @param fifo le fifo dans lequel on retire
* @param val un pointeur dans lequel on va écrire la valeur en tête du fifo
* @return 0 si tout va bien, -1 si le fifo est vide */
int fifo_dequeue(fifo_t* fifo, int* val) {
  if (fifo->debut < 0)
    return -1; /* la file est vide : erreur */

  *val = (fifo->contenu)[fifo->debut];
  if (fifo->debut == fifo->fin) {
    /* si c'était le dernier élement de la file,
     * on doit retransformer en la file vide */
    fifo->debut = -1;
    fifo->fin = -1;
  } else
    fifo->debut = (fifo->debut + 1) % BUFLEN;
  return 0;

}

int main(int argc, char *argv[])
{
  fifo_t mafifo;
  int val;
  int cpt = 0;
  init_fifo(&mafifo);

  fifo_enqueue(&mafifo, 1);
  fifo_enqueue(&mafifo, 2);

  while (fifo_dequeue(&mafifo, &val) >= 0) {
    printf("val=%d, attendu=%d\n", val, ++cpt);
  }
  printf("retour=%d, attendu=-1\n", fifo_dequeue(&mafifo, &val));

  fifo_enqueue(&mafifo, 3);
  fifo_enqueue(&mafifo, 4);
  printf("retour=%d, attendu=0\n", fifo_enqueue(&mafifo, 5));
  printf("retour=%d, attendu=0\n", fifo_enqueue(&mafifo, 6));
  printf("retour=%d, attendu=0\n", fifo_enqueue(&mafifo, 7));
  printf("retour=%d, attendu=-1\n", fifo_enqueue(&mafifo, 8));

  while (fifo_dequeue(&mafifo, &val) >= 0) {
    printf("val=%d, attendu=%d\n", val, ++cpt);
  }
  return EXIT_SUCCESS;
}
