{ pkgs ? import <nixpkgs> {  } }:
let
  derivation = pkgs.callPackage ./derivation.nix {};
in
pkgs.stdenv.mkDerivation {
  name = "env-${derivation.name}";

  buildInputs = derivation.buildInputs ++  (with pkgs; [ clang-tools gdb valgrind ]);

  nativeBuildInputs = derivation.nativeBuildInputs;
}
